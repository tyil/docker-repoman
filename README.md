# `repoman` Docker Image

This repository contains the Dockerfile to build a Docker image containing the
[`repoman`](https://wiki.gentoo.org/wiki/Repoman) utility from Gentoo.

This image is intended to be used with GitLab CI, or other CI tools, to easily
run a number of QA checks on overlay projects. The GitLab CI is setup to build
a new image every week.
