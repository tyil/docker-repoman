FROM gentoo/stage3-amd64 AS build

ADD http://distfiles.gentoo.org/snapshots/portage-latest.tar.bz2 /tmp/portage.tar.bz2

RUN mkdir -p /var/db/repos/gentoo
RUN tar xjf /tmp/portage.tar.bz2 --strip-components 1 -C /var/db/repos/gentoo
RUN emerge -1 dev-vcs/git
RUN emerge -1 app-portage/repoman

FROM gentoo/stage3-amd64

COPY --from=build /usr /usr
COPY --from=build /var/db/repos/gentoo /var/db/repos/gentoo
